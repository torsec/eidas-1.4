# eIDAS Node v1.4

This is a mirror for the official eIDAS Node v1.4 available on the
[CEF Digital] web site.

[CEF Digital]: https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS-Node+-+Current+release
